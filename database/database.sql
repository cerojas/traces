DROP database IF EXISTS isw613_questionnaires;
CREATE database isw613_questionnaires;

CREATE user if NOT EXISTS 'isw613_user'@'localhost' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON isw613_questionnaires.* TO 'isw613_user'@'localhost';
FLUSH PRIVILEGES;

USE isw613_questionnaires;

CREATE TABLE users (
 id INT NOT NULL AUTO_INCREMENT,
 username VARCHAR(45) NOT NULL,
 passwd VARCHAR(45) NOT NULL,
 PRIMARY KEY (id));


CREATE TABLE questionnaires (
 id INT NOT NULL AUTO_INCREMENT,
 description VARCHAR(150) NOT NULL,
 long_description VARCHAR(400) NOT NULL,
 PRIMARY KEY (id));

CREATE TABLE questions (
 id INT NOT NULL AUTO_INCREMENT,
 questionnaire_id INT NOT NULL,
 question_text VARCHAR(400) NOT NULL,
 PRIMARY KEY (id),
 INDEX questions_questionnaire_id_fkey_idx (questionnaire_id ASC),
 CONSTRAINT questions_questionnaire_id_fkey
 FOREIGN KEY (questionnaire_id)
 REFERENCES questionnaires (id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION);

CREATE TABLE answers (
 id INT NOT NULL AUTO_INCREMENT,
 question_id INT NOT NULL,
 number INT NOT NULL,
 answer_text VARCHAR(400) NOT NULL,
 answer_points INT NOT NULL,
 PRIMARY KEY (id),
 INDEX answers_question_id_fkey_idx (question_id ASC),
 CONSTRAINT answers_question_id_fkey
 FOREIGN KEY (question_id)
 REFERENCES questions (id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION);
 
 CREATE TABLE users_questionnaires (
 id INT NOT NULL AUTO_INCREMENT,
 user_id INT NOT NULL,
 questionnaire_id INT NOT NULL,
 result INT NOT NULL,
 PRIMARY KEY (id),
 INDEX users_questionnaires_user_id_fkey_idx (user_id ASC),
 INDEX users_questionnaires_questionnaire_id_fkey_idx (questionnaire_id ASC),
 CONSTRAINT users_questionnaires_user_id_fkey
 FOREIGN KEY (user_id)
 REFERENCES users (id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 CONSTRAINT users_questionnaires_questionnaire_id_fkey
 FOREIGN KEY (questionnaire_id)
 REFERENCES questionnaires (id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION);

CREATE TABLE results (
 id INT NOT NULL AUTO_INCREMENT,
 questionnaire_id INT NOT NULL,
 min_value INT NOT NULL,
 max_value INT NOT NULL,
 feedback VARCHAR(400) NOT NULL,
 PRIMARY KEY (id),
 INDEX results_questionnaire_id_fkey_idx (questionnaire_id ASC),
 CONSTRAINT results_questionnaire_id_fkey
 FOREIGN KEY (questionnaire_id)
 REFERENCES questionnaires (id)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION);

INSERT INTO users VALUES (1,"cerojas", "123");
