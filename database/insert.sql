
//insert para las respuestas del test de trabajo
INSERT INTO answers VALUES  
(1,1,1,'Colabora si es necesario pero bajo la direccion de otra persona',15),
(2,1,2,'Si es posible evito participar en la toma de decisiones',10),
(3,1,3,'Me gusta enfrentarme al reto',20),
(4,2,1,'Me bloqueo e intento evitarlo por todos los medios',10),
(5,2,2,'Si es necesario lo hago pero no me entusiasma',15),
(6,2,3,'No tengo ningun problema, me gusta hablar en publico',20),
(7,3,1,'Intento pasar desapercibido y que no se note mi presencia',10),
(8,3,2,'Me encanta,poder escuchar el punto de vista y las opiniones de los demás',20),
(9,3,3,'No suelo estar cómodo pero salgo del paso con esfuerzo',15),
(10,4,1,'Prefiero escuchar y no dar mi opinion',10),
(11,4,2,'Me parece importante exponer mi punto de vista por si ayuda',20),
(12,4,3,'Solo hablo en el caso de que no me convenza la opinion de los demas',15),
(13,5,1,'Siempre me adapto a lo que decidan los demas',10),
(14,5,2,'Normalmente acabamos haciendo lo que yo prefiero',20),
(15,5,3,'Suelo opinar pero casi siempre se acaba haciendo lo que dicen los demas',15);



//insert para las preguntas del test de trabajo

INSERT INTO questions VALUES (1,1,'Si surge la necesidad de tener que organizar equipos...'),
(2,1,'A la hora de hablar en público....'),
(3,1,'Cuando participo en una reunión de más de 4 personas'),
(4,1,'A la hora de exponer tu punto de vista ante un grupo de personas ?'),
(5,1,'A la hora de organizar planes con lo amigos..');
//insert para las respuestas del test de salud

INSERT INTO questions VALUES
(6,2,"Tengo periodos en los que tiendo a hablar mucho más rápido de lo que habitualmente lo hago?"),
(7,2,"Mi autoestima varia entre tener grandes dudas sobre mi mismo hasta un exceso de confianza igual de grande?"),
(8,2,"He tenido períodos de gran optimismo y otros períodos de pesimismo igualmente grandes?"),
(9,2,"He tenido momentos en los que me he sentido exaltado y deprimido al mismo tiempo?"),
(10,2,"Tengo períodos de embotamiento mental y otros períodos de pensamiento muy creativo?");


//insert para las respuestas del test de Amistad
INSERT INTO questions VALUES
(11,3,"Un amigo traicionó tu confianza. ¿Qué haces?"),
(12,3,"Serias capaz de renunciar a algo importante por ayudar a un amigo en apuros?"),
(13,3,"Tienes el mismo nivel de confianza con todos tus amigos?"),
(14,3,"Si descubres que alguno de tus amigos esta enamorado de la misma persona que ti. entonces:"),
(15,3," Pasar una tarde charlando con un buen amigo, ¿es uno de los mejores placeres de la vida?");





//insert para las respuestas del test de salud


INSERT INTO answers VALUES  
(16,6,1,'Nunca',5),
(17,6,2,'Solo un poco',10),
(18,6,3,'Algunos veces',15),
(19,6,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES  
(20,7,1,'Nunca',5),
(21,7,2,'Solo un poco',10),
(22,7,3,'Algunos veces',15),
(23,7,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES 
(24,8,1,'Nunca',5),
(25,8,2,'Solo un poco',10),
(26,8,3,'Algunos veces',15),
(27,8,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES 
(28,9,1,'Nunca',5),
(29,9,2,'Solo un poco',10),
(30,9,3,'Algunos veces',15),
(31,9,4,'Muy frecuentemente',20);
INSERT INTO answers VALUES 
(32,10,1,'Nunca',5),
(33,10,2,'Solo un poco',10),
(34,10,3,'Algunos veces',15),
(35,10,4,'Muy frecuentemente',20);


//insert para las respuestas del test de amistad 

INSERT INTO answers VALUES 
(36,11,1,'No le hablo nunca más',10),
(37,11,2,'Le pido explicaciones, y quizás le dé otra chance',15),
(38,11,3,'Seguro lo perdono: la amistad es sagrada',20),
(39,12,1,'Depende; debería pensar en las consecuencias',10),
(40,12,2,'Sí, pero buscaría la manera de no salir perjudicado',15),
(41,12,3,'Sí, siempre',20),
(42,13,1,'Soy igual con todos, quizás porque con ninguno tengo una relación profunda',10),
(43,13,2,'Tengo un solo amigo del corazón, y muchos con los que tengo una excelente relación.',15),
(44,13,3,'No hago diferencia entre quienes considero mis amigos',20),
(45,14,1,'Sin darle explicación, empiezo a dejar de ver a mí amigo',10),
(46,14,2,'Hablo con el para aclarar la situación',15),
(47,14,3,'Me retiro de la pugna, sin revelar mis sentimientos.',20),
(48,15,1,'A veces',10),
(49,15,2,'La Mayoria',15),
(50,15,3,'Si,siempre',20);

//insert la retroalimentacion test de trabajo
INSERT INTO results VALUES 
(1,1,25,50,'No tienes madera de líder
No demuestras tener casi ningún rasgo que denote que eres un líder. Prefieres que otras personas decidan por 
ti y te gusta pasar desapercibido en la mayoría de las situaciones. Pero ya sabes el refrán …”querer es poder”así que con empeño y
 determinación podrías reunir todas las cualidades necesarias para ser considerado un buen líder.'),
(2,1,51,70,'Puedes llegar a ser un líder
Aunque no puedas ser reconocido como un líder nato, demuestras poseer la capacidad de implicarte y 
actuar como tal en ciertas ocasiones así que ¡no está todo perdido! Si de verdad quieres convertirte en 
un buen líder, detecta las carencias que tienes y trabájalas para conseguir tus objetivos en la vida'),
(3,1,71,100,'Eres un auténtico líder y no puedes disimularlo. La gente de tu entorno te respeta y reconoce como una persona
de referencia en la que confiar. Tu comportamiento tanto dentro como fuera del trabajo no deja lugar a duda, 
sabes dirigir, coordinar, convencer ayudar y motivar a todas las personas que tienes alrededor.');


//insert la retroalimentacion de salud

INSERT INTO results VALUES 
(4,2,25,50,'Trastorno Bipolar leve
  En muchos casos, estos síntomas no afectan 
el funcionamiento normal de una persona. Consulta con un profesional de salud mental
si estás experimentando sentimientos depresivos y/o dificultades en el funcionamiento cotidiano.'),
(5,2,51,70,'Trastorno Bipolar con síntomas moderados. Las personas que han respondido de 
 manera similar normalmente califican para un diagnóstico de trastorno bipolar I o II, y han recibido
 tratamiento profesional para este trastorno. '),
 (6,2,71,100,'Trastorno bipolar con síntomas graves. . Las personas que han 
 respondido de manera similar normalmente califican para un diagnóstico de trastorno bipolar I o II,
 y han recibido tratamiento profesional para este trastorno. 
 Sería beneficioso que busques un diagnóstico más profundo de un profesional de la salud mental.');

 //insert de retroalimentacion de Amistad

INSERT INTO results VALUES 
(7,3,25,50,'La amistad no se encuentra en la cima de tus prioridades en la vida. Eres bastante escéptico y te ilusionas poco.
 Aunque tengas algunos amigos, mantienes una relación más bien fría y superficial. Si bien hay otras cosas importantes,
 la amistad es un bien muy valioso que quizás estés perdiendo. Intenta ser más abierto.'),
(8,3,51,70,' Te gusta cultivar la amistad. Te gusta dar y recibir afecto, por lo que suelen considerarte confiable y buen amigo. 
 Pero tu eres selectivo: solo quienes cumplen ciertos requisitos son parte de tu circulo intimo. Tienes muchos conocidos y
 pocos amigos. Además prefieres escuchar antes que hablar, y no confías a cualquiera tus secretos. '),
 (9,3,71,100,'La amistad es para ti un valor fundamental, tan importante y gratificante como el amor. 
 Te sientes vivo, pleno y aceptado solamente estando entre tu grupo de amigos. Puedes dejar todo por ellos. Una advertencia: estar enamorado 
 de la amistad puede generarte cierta dependencia. Como en todo en la vida, busca el equilibrio.');


//insert lista de cuestionarios

INSERT INTO questionnaires VALUES 
(1,"Test de Trabajo", 'Test para saber si soy líder!!Si quieres descubrir si eres un auténtico líder , contesta a las preguntas del siguiente test de personalidad sobre liderazgo.'),
(2,"Test de bipolaridad", 'Realizando el siguiente test de bipolaridad podrás tener una orientación de si tus comportamientos o los de una persona cercana son normales o son debido a un trastorno de bipolaridad.'),
(3,"Test de Amistad", 'Hay quienes consideran a los amigos un verdadero tesoro y mantienen sus conversaciones más profundas con ellos. Otros sólo los tienen cerca cuando presenta algún problema. Responde con sinceridad a estas preguntas y descubre que valor le das a la amistad.');










