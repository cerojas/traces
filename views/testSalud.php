<?php include "../config.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cuestionario de Salud</title>
     <!--framework css bulma-->
    <link rel="stylesheet" href="/css/bulma.min.css">
     <!--libreria de javascript-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<!--mostrar el navbar-->
<nav class="navbar is-dark">
  <div class="navbar-brand">
    <a class="navbar-item" >
     <span>AvanSoftware</span>
    </a>
    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
<!--ejecutar el formulario logout-->
  <div class="navbar-end">
  <form action="../actions/logout.php" method="POST">
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">

          <input type="submit" value="Logout" id="loginbutton" class="button  is-danger is-medium " >
          
          </p>
         </form>
        </div>
      </div>
    </div>
   </nav>
   <br>
   <br>

   <h1 style = "color: red; font-size:200%">Test de Salud</h1>
    
  <?php
// Conexion a la base de datos 
include "../actions/conexion.php";

// Validar si ya contestó el test seleccionado

$user = $_SESSION['user'];
$useract = $user['id'];
$id = $_GET['id'];
$validacion = $mysqli->query("SELECT * FROM users_questionnaires WHERE user_id=" . $user['id'] . " and  questionnaire_id=" . $id);

?>
<?php
//seleccionar los datos de la base de datos
$sql = $mysqli->query("SELECT users.id, users.username, questionnaires.description, results.feedback,users_questionnaires.questionnaire_id,
                      users_questionnaires.result, results.min_value, results.max_value
                      FROM ((users_questionnaires
                      INNER JOIN  users ON users.id=users_questionnaires.user_id
                      INNER JOIN  questionnaires ON questionnaires.id = users_questionnaires.questionnaire_id
                      INNER JOIN  results ON questionnaires.id = results.questionnaire_id))
                      where users.id=" . $useract . "
                      AND users_questionnaires.questionnaire_id=" . $id . "
                      and results.min_value <= users_questionnaires.result 
                      and results.max_value >= users_questionnaires.result");
//validar si el usuario ya contesto el cuestionario para mostrar el resultado
if ($validacion->num_rows === 1) : ?>
<div class="container">
  <br>
    <?php foreach ($sql as $opcion) : ?>
      <h5>Test: <?php echo $opcion['description']; ?></H5>
     <br>
      <h5>Resultado del Test: <?php echo $opcion['result']. " puntos"; ?> </h5>
      <div class="card">
        <div class="card-body">
          <h5>Retroalimentacion del test:</h5>
          <br>
          <div class="p-3 mb-2 bg-dark text-white"><?php echo $opcion['feedback']; ?> </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
  <!--ejecutar el formulario resultados-->
  <?php
    else : ?>
<form action="/views/resultados.php" method="POST">
  <div class="container">
  <?php
  
    
    //selecciona las preguntas

    $questions = $mysqli->query("SELECT * FROM questions where questionnaire_id=" . $id);

    // recorre las preguntas
    while ($question = mysqli_fetch_array($questions)) {
     //selecciona las respuestas
      $answers = $mysqli->query("SELECT * FROM answers where question_id=" . $question['id'] . " order by number");
      ?>
<!--imprimo las preguntas-->
      <div class="row" >
      <br>
        <div class="">
          <div class="card">
            <div class="card-content">
              <?php echo $question['question_text']; ?>
            </div>
            
            <div class="card-content">
            
            
                <?php
                 //recorre las respuestas
                  while ($answer = mysqli_fetch_array($answers)) {

                    ?>
                     <!--lista las respuestas-->
                  
                 <li style="list-style:none;"><input  id="sel" name="sel" type="checkbox"  value="<?php echo $answer['answer_points'] ?>" > <?php echo "&nbsp;"; echo "&nbsp;";  echo $answer['answer_text'] ?> </li>
         
                <?php
                  }
                  ?>
             
            </div>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
    <br>
    <div class="row">
      <div class="col-md-12">
        <input type="hidden" id="totalSum" name="totalSum" />
        <input type="hidden" id="questionnaryid" name="questionnaryid" value="<?php echo $id ?>" />
        <button type="submit" class="button is-primary">Ir al resultado </button>
      </div>
    </div>
  </div>
</form>
</body>
</html>
<?php endif; ?>
<!--codigo de javascript que suma los puntos-->
<script>
  $(document).ready(function() {
    $("input[type=checkbox]").click(function() {
    var total = 0;
    $("input[type=checkbox]:checked").each(function() {
        total += parseFloat($(this).val());
    });

    $("#totalSum").val(total);
});
});
</script>
