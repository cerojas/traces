<?php include "../config.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <link rel="stylesheet" href="/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="layout-default">
<!--metodo para imprimir las alertas-->
<?php if ($_SESSION['access_denied'] ?? null) : ?>
  <div class="notification is-warning">
  <strong></strong><?= $_SESSION['access_denied']; ?>
  <?php $_SESSION['access_denied'] = null; ?>
  <?php endif; ?>
  <button class="delete" > </button> <script>  
  //metodo javascript para cerrar la notificacion
  document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    $notification = $delete.parentNode;
    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});</script>
</div>
    <section class="hero is-fullheight is-medium is-primary is-bold"><!--coloca el contenedor en el centro de la pantalla-->
        <div class="hero-body"><!--coloca el contenedor en el centro de la pantalla-->
          <div class="container">
            <div class="columns is-centered"> <!--centra las columnas en la pagina-->
              <article class="card is-rounde"> <!--contendor blanco que rodea los inputs-->
                <div class="card-content"><!--Ajusta el contedor blanco con margenes-->
                  <h1 class="title">
                    <h1><strong>Registro</strong></h1>
                    <img src= "/img/logo.png" alt="logo" width="200">
                  </h1>
                  <div class="field">
                  <form action="../actions/register.php" method="POST">
                    <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="text" placeholder="Usuario" id="username" name="username"><!--le damos color al input-->
                    </div>
                  </div>
                  <div class="field"> <!--clase para los inputs-->
                    <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="password" placeholder="Contraseña" id="confirmarpass" name="confirmarpass"><!--le damos color al input-->
                    </div>
                  </div>
                  <div class="field"> <!--clase para los inputs-->
                  <div class="control "> <!--con esta clase le damos el espacio para la imagen en el input-->
                    <input class="input is-primary" type="password" placeholder="Confirmar Contraseña" id="password" name="password"><!--le damos color al input-->
                  </div>
                </div>
                  <div class="field"> <!--clase para los inputs-->
                         <input type="submit" value="Registrar" id="register" class="button  is-primary is-medium " >
                        <a id="link"href="/inicio.php"> <input type="button" value="Cancelar" id="cancel" class="button  is-danger is-medium " ></a>
                        </form>
                  </div>
                </div>
              </article>
            </div>
          </div>
    </div>
  </section>
</body>
</html>