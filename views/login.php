<?php include "../config.php"; ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
  <!--  <LINK REL=StyleSheet HREF="TicoRidesLogin.css" TYPE="text/css">-->
    <link rel="stylesheet" href="/css/bulma.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
     

  </head>
  <body class="layout-default">
  <!--metodo para imprimir las alertas-->

  <?php if ($_SESSION['access_denied'] ?? null) : ?>
  <div class="notification is-warning">
  <strong></strong><?= $_SESSION['access_denied']; ?>
  <?php $_SESSION['access_denied'] = null; ?>
  <?php endif; ?>
  <button class="delete" > </button> <script>  
  //metodo javascript para cerrar la notificacion
  document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    $notification = $delete.parentNode;
    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});</script>
</div>

  <section class="hero is-fullheight is-medium is-primary is-bold"><!--coloca el contenedor en el centro de la pantalla-->
        <div class="hero-body"><!--coloca el contenedor en el centro de la pantalla-->
          <div class="container">
            <div class="columns is-centered"> <!--centra las columnas en la pagina-->
              <article class="card is-rounde"> <!--contendor blanco que rodea los inputs-->
                <div class="card-content"><!--Ajusta el contedor blanco con margenes-->
                  <h1 class="title">
                    <img src= "/img/logo.png" alt="logo" width="200">
                  </h1>
                  <!--form para ejecutar el formulario checklogin-->
                  <form action="../actions/checklogin.php" method="POST">
                  
                  <div class="field">
                    <label class="label">Usuario</label>
                    <div class="control has-icons-left">
                      <input class="input is-primary" type="text" placeholder="Usuario" id="username" name="username">
                      <span class="icon is-left">
                         <i class="zmdi zmdi-account zmdi-hc-lg"></i>
                      </span>
                    </div>
                  </div>
                  <div class="field"> <!--clase para los inputs-->
                    <label class="label">Contraseña</label>
                    <div class="control has-icons-left"> <!--con esta clase le damos el espacio para la imagen en el input-->
                      <input class="input is-primary" type="password" placeholder="Contraseña"  id="password" name="password"> <!--le damos color al input-->
                      <span class="icon is-left"><!--clase para colocar la imagen-->
                         <i class="zmdi zmdi-key zmdi-hc-lg"></i><!--imagen de material design-->
                      </span>
                    </div>
                  </div>
                  <br>
                  <div class="columns is-centered">
                  <div class="control ">
                         <input type="submit" value="Login" id="loginbutton" class="button  is-primary is-medium " >
                         <a id="link"href="/inicio.php"> <input type="button" value="Cancelar" id="cancel" class="button  is-danger is-medium " ></a>
                         </form>
                  </div>
                  </div>
                  </div>
                </div>
              </article>
            </div>
          </div>
    </div>
  </section>
</body>
</html>