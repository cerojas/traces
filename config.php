<?php
//codigo para controlas las sessiones
if(!isset($_SESSION)){
  session_start();
}
  $public = "/";
  $debug = true;


  $hostname = $_SERVER['HTTP_HOST'];
  $user       = $_SESSION['user'] ?? null;
  $scriptName = $_SERVER['SCRIPT_NAME'];

  if ($debug) {
    ini_set("display_errors", 1);
    echo '<div class="alert alert-secondary" role="alert">';
   // echo "<b>ScriptName: </b>" . $scriptName;
    echo '</div>';
  }