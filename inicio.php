<?php include "../config.php"; ?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>
    <link rel="stylesheet" href="css/bulma.min.css">
</head>
<body>
<nav class="navbar is-dark">
  <div class="navbar-brand">
    <a class="navbar-item" >
     <span>Home</span>
    </a>
    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">
          <a class="bd-tw-button button" href="views/login.php">
             
              <span>
                Login
              </span>
            </a>
          </p>
          <p class="control">
            <a class="bd-tw-button button" href="views/registro.php">
             
              <span>Register</span>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</nav>

<section class="hero is-fullheight is-medium is-white is-bold"><!--coloca el contenedor en el centro de la pantalla-->
        <div class="hero-body"><!--coloca el contenedor en el centro de la pantalla-->
          <div class="container">
            <div class="columns is-centered"> <!--centra las columnas en la pagina-->
             
                    <img src= "img/checklist.jpg" alt="logo" width="400">
                    
                    </div>
                  </div>
                  </div>

                </section>

</body>
</html>