<?php
include_once '../config.php';
use mysqli;

$username = $_POST['username'] ?? '';
$user = $_SESSION['user'] ?? null;
$password = $_POST['password'] ?? '';
$confirmarpass = $_POST['confirmarpass'] ?? '';

// Parámetros de base de datos
$dbHost     = '127.0.0.1';
$dbUser     = 'root';
$dbPassword = 'root';
$dbName     = 'isw613_questionnaires';
$dbPort     = '3306';

// Realiza la conexión a BD
$mysqli = new mysqli( $dbHost, $dbUser, $dbPassword, $dbName, $dbPort );
if ( $mysqli->connect_errno ) {
    echo 'Error de conexión de MySQL: (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error;
}

//confirma si el usuario ya ha sido registrado
$sql = 'SELECT id, username FROM users ' .
" WHERE username = '". $username . "'" ;

$datasource = $mysqli->query( $sql );
if ( $datasource->num_rows > 0 ) {
    $_SESSION['user'] = $datasource->fetch_assoc();
    $_SESSION['access_denied'] = 'El usuario ya se encuentra registrado';
    header( 'Location: ' . $public . 'views/registro.php' );

} else {

    //confirma si las contraseñas coinciden

    if ( $password == $confirmarpass ) {
//insertar usuarios
        $sql = "INSERT INTO users (username, password)
    VALUES ('$username', '$password')";

        if ( $mysqli->query( $sql ) === TRUE ) {
            echo 'Nuevo usuario creado correctamente';
            header( 'Location: ' . $public . 'views/login.php' );
            $_SESSION['access_denied'] = 'Usuario registrado correctamente';
        } else {
            //valida si usuario ya esta ingresado
            echo 'Error: ' . $sql . '<br>' . $mysqli->error;
            header( 'Location: ' . $public . 'views/registro.php' );
            $_SESSION['access_denied'] = 'El usuario ya se encuentra registrado';
        }

        $mysqli->close();

    } else {
        //imprime en pantalla si las contraseñas no coinciden
        header( 'Location: ' . $public . 'views/registro.php' );
        $_SESSION['access_denied'] = 'Las contraseñas no coinciden';

    }

}

?>
